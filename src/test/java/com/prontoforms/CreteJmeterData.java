package com.prontoforms;

import com.prontoforms.pesdriver.LoginPage;
import com.prontoforms.selenium.pages.LoginPageImpl;
import org.testng.annotations.Test;

public class CreteJmeterData extends AbstractTest {

	@Test
	public void createJmeterDataForApiregression() {
		LoginPage pes = new LoginPageImpl(driver);

		pes.loginAsSuperAdmin(superAdminLogin, superAdminPassword)
				.goToSalesTeamsPage()
				.openUpdateTeamPage("TrueContext Sales")
				.addAllBillingTiers();
	}
}
package com.prontoforms.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractPageImpl {

	private WebDriverWait wait = null;
	public WebDriver driver = null;
	public String serverAddress = "http://localhost:80";

	public AbstractPageImpl(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver, 10);
		PageFactory.initElements(driver, this);
	}

	public void waitToBeClickable(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void waitToBeVisible(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public void mouseOver(WebElement element) {
		new Actions(driver).moveToElement(element).build().perform();
	}

	public void clickOn(WebElement element) {
		waitToBeClickable(element);
		element.click();
	}

	public void clickOn(By by) {
		WebElement element = driver.findElement(by);
		clickOn(element);
	}
}

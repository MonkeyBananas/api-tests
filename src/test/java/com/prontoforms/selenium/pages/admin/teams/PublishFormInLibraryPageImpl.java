package com.prontoforms.selenium.pages.admin.teams;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PublishFormInLibraryPageImpl extends ProntoTeamToolbar {

	@FindBy(id = "submitButton")
	private WebElement updateButton;

	public PublishFormInLibraryPageImpl(WebDriver driver) {
		super(driver);
	}

	public ConcreteFormPageImpl confirmPublish() {
		clickOn(updateButton);
		return new ConcreteFormPageImpl(driver);
	}
}

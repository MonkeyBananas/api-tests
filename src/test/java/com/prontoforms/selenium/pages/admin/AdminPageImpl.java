package com.prontoforms.selenium.pages.admin;

import com.prontoforms.pesdriver.admin.AdminPage;
import com.prontoforms.pesdriver.admin.AdminPanel;
import org.openqa.selenium.WebDriver;

//This is http://localhost/admin page.
public class AdminPageImpl extends AdminPanelImpl implements AdminPage {

	public AdminPageImpl(WebDriver driver) {
		super(driver);
	}
}

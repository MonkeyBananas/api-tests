package com.prontoforms.selenium.pages.admin.teams;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConcreteDataSourcePageImpl extends ProntoTeamToolbar {

	public ConcreteDataSourcePageImpl(WebDriver driver) {
		super(driver);
	}

	@FindBy(xpath = "//div[contains(@class, 'menu_white')]")
	private WebElement dataSourceActionsDropDown;
	@FindBy(partialLinkText = "Publish in Library")
	private WebElement publishInLibraryButton;

	public PublishDataSourceInLibraryPageImpl publishDataSourceInLibrary(){
		waitToBeVisible(dataSourceActionsDropDown);
		mouseOver(dataSourceActionsDropDown);
		clickOn(publishInLibraryButton);

		return new PublishDataSourceInLibraryPageImpl(driver);
	}
}

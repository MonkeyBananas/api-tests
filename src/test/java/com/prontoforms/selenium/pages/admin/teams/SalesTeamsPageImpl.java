package com.prontoforms.selenium.pages.admin.teams;

import com.prontoforms.pesdriver.admin.teams.EditSalesTeamPage;
import com.prontoforms.pesdriver.admin.teams.SalesTemsPage;
import com.prontoforms.selenium.pages.admin.AdminPanelImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SalesTeamsPageImpl extends AdminPanelImpl implements SalesTemsPage {

	public SalesTeamsPageImpl(WebDriver driver) {
		super(driver);
	}

	public EditSalesTeamPage openUpdateTeamPage(String teamName) {
		mouseOverGearButtonForTeam(teamName);
		return clickUpdateTeamBtn(teamName);
	}

	private EditSalesTeamPage clickUpdateTeamBtn(String teamName) {
		By updateBtn = By.xpath("//tr[.//*[contains(text(), '" + teamName + "')]]//li[@class='icon_edit'][a[text()='Update']]");
		clickOn(updateBtn);
		return new EditSalesTeamPageImpl(driver);
	}

	private void mouseOverGearButtonForTeam(String teamName) {
		By gearBtnSelector = By.xpath("//tr[.//*[contains(text(), '" + teamName + "')]]//div[contains(@class, 'menu-opener')]");
		WebElement gearBtn = driver.findElement(gearBtnSelector);
		waitToBeVisible(gearBtn);
		mouseOver(gearBtn);
	}
}

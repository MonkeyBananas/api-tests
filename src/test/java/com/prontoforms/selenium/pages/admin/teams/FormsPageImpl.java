package com.prontoforms.selenium.pages.admin.teams;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FormsPageImpl extends ProntoTeamToolbar {

	@FindBy(xpath = "//div/*[h1[text()='Forms']]/div[contains(@class, 'menu')]")
	private WebElement formsDropdownTriangle;
	@FindBy(linkText = "Upload a V2 Form")
	private WebElement uploadV2FormBtn;

	public FormsPageImpl(WebDriver driver) {
		super(driver);
	}

	public ConcreteFormPageImpl uploadV2Form(String filename) {
		waitToBeClickable(formsDropdownTriangle);
		mouseOver(formsDropdownTriangle);
		clickOn(uploadV2FormBtn);
		UploadAFormPageImpl uploadAFormPage = new UploadAFormPageImpl(driver);
		uploadAFormPage.uploadForm(filename);

		return new ConcreteFormPageImpl(driver);
	}
}

package com.prontoforms.selenium.pages.admin.teams;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConcreteFormSpacePageImpl extends ProntoTeamToolbar {

	@FindBy(linkText = "Forms")
	private WebElement formsIcon;

	public ConcreteFormSpacePageImpl(WebDriver driver) {
		super(driver);
	}

	public FormsPageImpl openForms() {
		clickOn(formsIcon);

		return new FormsPageImpl(driver);
	}
}

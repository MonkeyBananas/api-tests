package com.prontoforms.selenium.pages.admin.teams;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConcreteFormPageImpl extends ProntoTeamToolbar {

	@FindBy(id = "itemMenu")
	private WebElement formActionsDropdown;
	@FindBy(partialLinkText = "Publish in Library")
	private WebElement publishInLibraryButton;

	public ConcreteFormPageImpl(WebDriver driver) {
		super(driver);
	}

	public boolean isFormNameVisible(String name) {
		By formName = By.xpath("//h1[contains(text(), '" + name + "')]");
		try {
			WebElement teamNameElement = driver.findElement(formName);
			waitToBeVisible(teamNameElement);
			return teamNameElement.isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public PublishFormInLibraryPageImpl publishFormInLibrary() {
		waitToBeVisible(formActionsDropdown);
		mouseOver(formActionsDropdown);
		clickOn(publishInLibraryButton);

		return new PublishFormInLibraryPageImpl(driver);
	}
}

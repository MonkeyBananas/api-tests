package com.prontoforms.selenium.pages.admin.teams;

import com.prontoforms.selenium.pages.admin.AdminPanelImpl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Everything in "List" and "Create" dropdowns
 */
public class ProntoTeamToolbar extends AdminPanelImpl {

	@FindBy(xpath = "//div[contains(@class, 'menu-opener') and contains(text(), 'Create')]")
	private WebElement createButton;
	@FindBy(xpath = "//div[contains(@class, 'menu-opener') and contains(text(), 'List')]")
	private WebElement listButton;
	@FindBy(linkText = "FormSpace")
	private WebElement createFormSpaceLink;
	@FindBy(linkText = "FormSpaces")
	private WebElement listFormSpacesButton;
	@FindBy(linkText = "Group")
	private WebElement groupButton;

	public ProntoTeamToolbar(WebDriver driver) {
		super(driver);
	}

	public FormSpaceCreationPageImpl openCreateFormSpacePage() {
		waitToBeClickable(createButton);
		mouseOver(createButton);
		clickOn(createFormSpaceLink);

		return new FormSpaceCreationPageImpl(driver);
	}

	public FormSpacesPageImpl openFormSpacesPage() {
		waitToBeVisible(listButton);
		mouseOver(listButton);
		clickOn(listFormSpacesButton);

		return new FormSpacesPageImpl(driver);
	}

	public GroupsPageImpl openGroupsPage() {
		waitToBeClickable(createButton);
		mouseOver(createButton);
		clickOn(groupButton);

		return new GroupsPageImpl(driver);
	}
}

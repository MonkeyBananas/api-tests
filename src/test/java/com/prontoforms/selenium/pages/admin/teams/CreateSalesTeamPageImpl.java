package com.prontoforms.selenium.pages.admin.teams;

import com.prontoforms.selenium.pages.admin.AdminPanelImpl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class CreateSalesTeamPageImpl extends AdminPanelImpl {

	@FindBy(id = "name")
	private WebElement nameField;
	@FindBy(id = "teamVersion")
	private WebElement teamVersionSelector;
	@FindBy(id = "billingSystemIdsAvailable")
	private WebElement billingSystemSelector;
	@FindBy(id = "billingSystemIdsAddElements")
	private WebElement addBillingSystemsButton;
	@FindBy(id = "ApiBillingSystemIdsAddElements")
	private WebElement addApiBillingsysTemsButton;
	@FindBy(id = "billingTierIdsAddElements")
	private WebElement addBillingTiersButton;
	@FindBy(id = "ApiBillingSystemIdsAvailable")
	private WebElement apiBillingSystemSelector;
	@FindBy(id = "billingTierIdsAvailable")
	private WebElement billingTierSelector;
	@FindBy(id = "submitButton")
	private WebElement submitButton;

	public CreateSalesTeamPageImpl(WebDriver driver) {
		super(driver);
	}

	public void createSalesTeam(String salesTeamName) {
		waitToBeClickable(nameField);
		nameField.sendKeys(salesTeamName);
		new Select(teamVersionSelector).selectByValue("V2");
		new Select(billingSystemSelector).selectByVisibleText("Truecontext");
		new Select(apiBillingSystemSelector).selectByVisibleText("Truecontext");
		Select billingTierSelectorSelector = new Select(billingTierSelector);
		billingTierSelectorSelector.getOptions().forEach(option -> {
			billingTierSelectorSelector.selectByVisibleText(option.getText());
		});
		clickOn(addBillingSystemsButton);
		clickOn(addApiBillingsysTemsButton);
		clickOn(addBillingTiersButton);
		clickOn(submitButton);
	}
}

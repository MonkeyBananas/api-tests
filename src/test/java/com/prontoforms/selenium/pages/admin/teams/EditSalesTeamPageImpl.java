package com.prontoforms.selenium.pages.admin.teams;

import com.prontoforms.pesdriver.admin.teams.ConcreteSalesTeamPage;
import com.prontoforms.pesdriver.admin.teams.EditSalesTeamPage;
import com.prontoforms.selenium.pages.admin.AdminPanelImpl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class EditSalesTeamPageImpl extends AdminPanelImpl implements EditSalesTeamPage {

	@FindBy(id = "billingTierIdsAvailable")
	private WebElement billingTiersSelector;
	@FindBy(id = "billingTierIdsAddElements")
	private WebElement addBilligTiersBtn;
	@FindBy(id = "submitButton")
	private WebElement updateBtn;

	public EditSalesTeamPageImpl(WebDriver driver) {
		super(driver);
	}

	public ConcreteSalesTeamPage addAllBillingTiers() {
		waitToBeVisible(billingTiersSelector);
		waitToBeClickable(addBilligTiersBtn);
		waitToBeClickable(updateBtn);

		Select billingTierSelectorSelect = new Select(billingTiersSelector);

		billingTierSelectorSelect.getOptions().stream().forEach(option -> {
			if (!option.isSelected()) {
				clickOn(option);
			}
		});
		clickOn(addBilligTiersBtn);
		clickOn(updateBtn);
		return new ConcreteSalesTeamPageImpl(driver);
	}
}

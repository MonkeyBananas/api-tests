package com.prontoforms.selenium.pages.admin;

import com.prontoforms.pesdriver.admin.AdminPage;
import com.prontoforms.pesdriver.admin.AdminPanel;
import com.prontoforms.selenium.pages.AbstractPageImpl;
import com.prontoforms.selenium.pages.admin.teams.CreateSalesTeamPageImpl;
import com.prontoforms.selenium.pages.admin.teams.SalesTeamsPageImpl;
import com.prontoforms.selenium.pages.admin.teams.TeamsPageImpl;
import org.openqa.selenium.WebDriver;

/**
 * Top panel when logged in as superadmin. Including "System Administration", "Devices", etc.
 */
public class AdminPanelImpl extends AbstractPageImpl implements AdminPanel {

	public AdminPanelImpl(WebDriver driver) {
		super(driver);
	}

	public TeamsPageImpl goToAllTeams() {
		driver.get(serverAddress + "/teams?q=&s=100");
		return new TeamsPageImpl(driver);
	}

	public SalesTeamsPageImpl goToSalesTeamsPage() {
		driver.get(serverAddress + "/salesteams?q=&s=100");
		return new SalesTeamsPageImpl(driver);
	}

	public CreateSalesTeamPageImpl openCreateSalesTeamPage() {
		driver.get(serverAddress + "/salesteams/new");
		return new CreateSalesTeamPageImpl(driver);
	}

	public AdminPage openAdminPage() {
		driver.get(serverAddress + "/admin");
		return new AdminPageImpl(driver);
	}
}

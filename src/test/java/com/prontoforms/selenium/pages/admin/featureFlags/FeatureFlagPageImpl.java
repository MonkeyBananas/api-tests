package com.prontoforms.selenium.pages.admin.featureFlags;

import com.prontoforms.selenium.pages.AbstractPageImpl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FeatureFlagPageImpl extends AbstractPage {

	@FindBy(id = "enabled")
	private WebElement enableCheckbox;
	@FindBy(xpath = "//input[@type = 'submit']")
	private WebElement submitButton;

	private String featureFlagCodeName;

	public FeatureFlagPageImpl(WebDriver driver, FeatureFlag flag) {
		super(driver);
		this.featureFlagCodeName = flag.toString();
		driver.get(serverAddress + "/togglz/edit?f=" + this.featureFlagCodeName);
	}

	public void enableGlobally() {
		waitToBeClickable(enableCheckbox);
		waitToBeClickable(submitButton);

		if (!enableCheckbox.isSelected()) {
			enableCheckbox.click();
			submitButton.click();
		}

	}
}

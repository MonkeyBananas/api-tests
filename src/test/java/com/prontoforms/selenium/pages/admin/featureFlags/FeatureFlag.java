package com.prontoforms.selenium.pages.admin.featureFlags;

public enum FeatureFlag {
	DEVICE_SEARCH_TAB("DeviceSearchTab"),
	DEVICE_SDK("DeviceSdk"),
	CSAT_FORMS("CsatForms"),
	CUSTOMER_FEEDBACK_DESTINATION("CustomerFeedbackDestination");

	private String name;

	FeatureFlag(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}
}

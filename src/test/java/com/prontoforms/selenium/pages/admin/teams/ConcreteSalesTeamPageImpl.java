package com.prontoforms.selenium.pages.admin.teams;

import com.prontoforms.pesdriver.admin.teams.ConcreteSalesTeamPage;
import com.prontoforms.selenium.pages.admin.AdminPanelImpl;
import org.openqa.selenium.WebDriver;

// http://localhost/salesteams/$teamId
public class ConcreteSalesTeamPageImpl extends AdminPanelImpl implements ConcreteSalesTeamPage {

	public ConcreteSalesTeamPageImpl(WebDriver driver) {
		super(driver);
	}
}

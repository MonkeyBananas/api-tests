package com.prontoforms.selenium.pages.admin.teams;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PublishDataSourceInLibraryPageImpl extends ProntoTeamToolbar {

	@FindBy(id = "submitButton")
	private WebElement updateButton;

	public PublishDataSourceInLibraryPageImpl(WebDriver driver) {
		super(driver);
	}

	public ConcreteDataSourcePageImpl confirmPublish() {
		clickOn(updateButton);

		return new ConcreteDataSourcePageImpl(driver);
	}
}

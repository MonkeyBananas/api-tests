package com.prontoforms.selenium.pages.admin.teams;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FormSpaceCreationPageImpl extends ProntoTeamToolbar {

	@FindBy(id = "name")
	private WebElement nameInputField;
	@FindBy(id = "pushUpdatesToDevices_NoPush")
	private WebElement noPushRadioButton;
	@FindBy(id = "submitButton")
	private WebElement submitButton;

	public FormSpaceCreationPageImpl(WebDriver driver) {
		super(driver);
	}

	public ConcreteFormSpacePageImpl createFormSpace(String name) {
		waitToBeClickable(nameInputField);

		nameInputField.sendKeys(name);
		clickOn(noPushRadioButton);
		clickOn(submitButton);

		return new ConcreteFormSpacePageImpl(driver);
	}
}

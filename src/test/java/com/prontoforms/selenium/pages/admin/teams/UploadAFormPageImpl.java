package com.prontoforms.selenium.pages.admin.teams;

import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.File;

public class UploadAFormPageImpl extends ProntoTeamToolbar {

	@FindBy(id = "formDocument")
	private WebElement formDocumentField;
	@FindBy(id = "submitButton")
	private WebElement uploadButton;

	public UploadAFormPageImpl(WebDriver driver) {
		super(driver);
	}

	public void uploadForm(String filename) {
		waitToBeClickable(formDocumentField);
		//		formDocumentField.sendKeys(new File("../FileUpLoad/" + filename).getAbsolutePath());
		File fileToUpload = new File("../FileUpLoad/" + filename);
		formDocumentField.sendKeys(FilenameUtils.normalize(fileToUpload.getAbsolutePath()).toString());
		clickOn(uploadButton);
	}
}

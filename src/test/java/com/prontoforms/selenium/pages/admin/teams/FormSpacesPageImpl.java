package com.prontoforms.selenium.pages.admin.teams;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FormSpacesPageImpl extends ProntoTeamToolbar {

	public FormSpacesPageImpl(WebDriver driver) {
		super(driver);
	}

	public DataSourcesListPageImpl openDataSourcesList(String formSpaceName) {
		By dataSourcesLink = By.xpath("(//tr[*/a[text() = '" + formSpaceName + "']]/*/a)[3]");
		clickOn(dataSourcesLink);

		return new DataSourcesListPageImpl(driver);
	}
}

package com.prontoforms.selenium.pages.admin.teams;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DataSourcesListPageImpl extends ProntoTeamToolbar {

	public DataSourcesListPageImpl(WebDriver driver) {
		super(driver);
	}

	public ConcreteDataSourcePageImpl openDataSource(String dataSourceName){
		By datasourceLink = By.linkText(dataSourceName);
		clickOn(datasourceLink);

		return new ConcreteDataSourcePageImpl(driver);
	}
}

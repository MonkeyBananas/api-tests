package com.prontoforms.selenium.pages.admin.teams;

import com.prontoforms.selenium.pages.admin.AdminPanelImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TeamsPageImpl extends AdminPanelImpl {

	public TeamsPageImpl(WebDriver driver) {
		super(driver);
	}

	public FamiliarityIndexPageImpl openProntoTeamTeamPage(String teamName) {
		By teamNameLinkLocator = By.linkText(teamName);
		WebElement teamNameLink = driver.findElement(teamNameLinkLocator);
		clickOn(teamNameLink);

		return new FamiliarityIndexPageImpl(driver);
	}
}

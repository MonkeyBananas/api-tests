package com.prontoforms.selenium.pages;

import com.prontoforms.pesdriver.LoginPage;
import com.prontoforms.pesdriver.admin.AdminPage;
import com.prontoforms.selenium.pages.admin.AdminPageImpl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

// http://localhost/security/login
public class LoginPageImpl extends AbstractPageImpl implements LoginPage {

	@FindBy(id = "login")
	private WebElement loginField;
	@FindBy(id = "pass")
	private WebElement passwordField;
	@FindBy(id = "submitButton")
	private WebElement loginButton;

	public LoginPageImpl(WebDriver driver) {
		super(driver);
		driver.get(serverAddress);
	}

	public AdminPage loginAsSuperAdmin(String username, String password) {
		waitToBeClickable(loginField);
		waitToBeClickable(passwordField);
		waitToBeClickable(loginButton);

		loginField.sendKeys(username);
		passwordField.sendKeys(password);
		loginButton.click();

		return new AdminPageImpl(driver);
	}

}

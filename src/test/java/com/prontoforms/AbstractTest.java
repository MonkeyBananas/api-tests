package com.prontoforms;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.testng.annotations.*;

public class AbstractTest {

	WebDriver driver;
	String superAdminLogin = "randd@truecontext.com";
	String superAdminPassword = "5620Sam!";

	@BeforeMethod
	public void setup() {
		driver = new ChromeDriver(new ChromeOptions().addArguments("headless"));
		//		driver = new ChromeDriver();
		//		driver = new FirefoxDriver(new FirefoxOptions().addArguments("-headless"));
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
		driver = null;
	}
}

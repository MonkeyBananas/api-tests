package com.prontoforms;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import io.restassured.http.Cookie;
import io.restassured.http.Cookies;
import io.restassured.http.Header;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;

import org.testng.annotations.*;

import java.util.List;

public class SampleTest {

    @Test
    public void makeSureThatGoogleIsUp() {
        given().when().get("http://www.google.com").then()
                .statusCode(200)
                .body(containsString("google"));
    }

    @Test
    public void makeSureJsonValueIsCorrect() {
        given().get("https://ifconfig.co/json").then()
                .body("city", equalTo("Kanata"))
                .and().statusCode(200);
    }

    @Test
    public void extractAndPrintHeaders() {
        List<Header> headers = given().get("https://ifconfig.co/json").then().extract().headers().asList();
        headers.forEach((System.out::println));
    }

    @Test
    public void extractAndSendCookie() {
        Cookies cookies = given().get("https://www.wikipedia.org/").then().extract().detailedCookies();

        cookies.forEach(System.out::println);

        List<Cookie> secondRequestCookies = given().cookies(cookies).get("https://www.wikipedia.org/").then()
                .extract().detailedCookies().asList();

        assertThat(secondRequestCookies.size(), is(0));
    }

    @Test
    public void responseTime() {
        long ms = given().get("https://speedtest.net").then().extract().time();
        System.out.println("Response time is " + ms + "ms");
    }

    @Test
    public void loginAsSuperAdmin() {
        String baseUrl = "http://localhost:80";
        String username = "randd@truecontext.com";

        ExtractableResponse<Response> loginPage = given().get(baseUrl + "/security/login")
                .then().extract();

        String csrfToken = loginPage.headers().get("X-CSRF-TOKEN").getValue();
        Cookies loginCookies = loginPage.detailedCookies();

        ExtractableResponse<Response> loginResponse = given()
                .queryParam("username", username)
                .queryParam("password", "5620Sam!")
                .queryParam("_csrf", csrfToken)
                .queryParam("rememberMe", "on")
                .cookies(loginCookies)
                .urlEncodingEnabled(true)
                .contentType("text/plain")
                .post(baseUrl + "/security/login/perform")
                .then().statusCode(302).extract();

        given().cookies(loginResponse.cookies())
                .when().get(baseUrl + loginResponse.header("Location"))
                .then().statusCode(200).body(containsString(username));

        given().cookies(loginResponse.cookies())
                .when().get(baseUrl + "/prontoteams/12-2/formspaces/191/forms")
                .then().statusCode(200).body(containsString("AllControlsForDispatchingV2 v1"));
    }

}
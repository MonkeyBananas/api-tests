package com.prontoforms.pesdriver.admin.teams;

import com.prontoforms.selenium.pages.admin.teams.ConcreteSalesTeamPageImpl;

public interface EditSalesTeamPage {

	ConcreteSalesTeamPage addAllBillingTiers();
}

package com.prontoforms.pesdriver.admin.teams;

public interface SalesTemsPage {

	EditSalesTeamPage openUpdateTeamPage(String teamName);
}

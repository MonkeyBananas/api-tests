package com.prontoforms.pesdriver.admin;

import com.prontoforms.pesdriver.admin.teams.SalesTemsPage;

public interface AdminPanel {

	//	TeamsPage goToAllTeams();
	//
	SalesTemsPage goToSalesTeamsPage();
	//
	//	CreateSalesTeamPage openCreateSalesTeamPage();

	AdminPage openAdminPage();
}

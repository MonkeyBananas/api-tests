package com.prontoforms.pesdriver;

import com.prontoforms.pesdriver.admin.AdminPage;

public interface LoginPage {

	AdminPage loginAsSuperAdmin(String username, String password);
}
